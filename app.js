//app.js
App({
  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    // 商店list
    var storesInfo = [
      {
        id: 's1',
        img: '/img/canteen/星苑.png',
        name: '星苑食堂',
        img1: '/img/dishes/捕获8.PNG',
        name1: '红烧肉',
        sale: 22
      },
      {
        id: 's2',
        img: '/img/canteen/明苑.png',
        name: '明苑食堂',
        img1: '/img/dishes/捕获9.PNG',
        name1: '回锅肉',
        sale: 35
      },
      {
        id: 's3',
        img: '/img/canteen/二三.png',
        name: '二三食堂',
        img1: '/img/dishes/捕获13.PNG',
        name1: '油爆大虾',
        sale: 41
      },
    ]
  
    // 菜单
    var dishes = [

      {
        sid: 's1',
        id: "s1_1",
        img: '/img/dishes/捕获8.PNG',
        name: '红烧肉',
        sale: 33,
        good: 44,
        oldCost: '30',
        perCost: 11.6,
        special: '2.93',
        dishNum: 0
      },
      {
        sid: 's1',
        id: "s1_2",
        img: '/img/dishes/捕获3.PNG',
        name: '鱼香肉丝',
        sale: 34,
        good: 21,
        oldCost: '30',
        perCost: 10.5,
        special: '2.93',
        dishNum: 0
      },
      {
        sid: 's2',
        id: "s2_1",
        img: '/img/dishes/捕获9.PNG',
        name: '回锅肉',
        sale: 36,
        good: 14,
        oldCost: '30',
        perCost: 9.9,
        special: '2.93',
        dishNum: 0
      },
      {
        sid: 's2',
        id: "s2_2",
        img: '/img/dishes/捕获7.PNG',
        name: '扁豆烧肉',
        sale: 37,
        good: 20,
        oldCost: '30',
        perCost: 14,
        special: '2.93',
        dishNum: 0
      },      
      {
        sid: 's3',
        id: "s3_1",
        img: '/img/dishes/捕获13.PNG',
        name: '油爆大虾',
        sale: 36,
        good: 14,
        oldCost: '30',
        perCost: 10,
        special: '2.93',
        dishNum: 0
      },
      {
        sid: 's3',
        id: "s3_2",
        img: '/img/dishes/捕获11.PNG',
        name: '土豆丝',
        sale: 37,
        good: 20,
        oldCost: '30',
        perCost: 10,
        special: '2.93',
        dishNum: 0
      }
    ]
    var orders = [{
      id: 'o1',
      sid: 's1',
      img: '/img/canteen/星苑.png',
      sname: '星苑',
      complete: '订单完成',
      orderLists: [{
        mid: "s1_1",
        name: '红烧肉',
        num: 1,
        cost: 11.6
      }, {
          mid: "s1_2",
          name: '鱼香肉丝',
          num: 1,
          cost: 10.5
        }],
      totalnum: 2,
      cost: 22.1
    },
    {
      id: 'o2',
      sid: 's2',
      img: '/img/canteen/明苑.png',
      sname: '明苑',
      complete: '订单完成',
      orderLists: [{
        mid: "s2_1",
        name: '回锅肉',
        num: 1,
        cost: 9.9
      }, {
        mid: "s2_3",
        name: '扁豆烧肉',
        num: 1,
        cost: 14
      }],
      totalnum: 2,
      cost: 23.9
    },
    {
      id: 'o3',
      sid: 's1',
      img: '/img/canteen/星苑.png',
      sname: '星苑',
      complete: '订单完成',
      orderLists: [{
        mid: "s1_2",
        name: '鱼香肉丝',
        num: 1,
        cost: 10.5
      }],
      totalnum: 1,
      cost: 10.5
    },
    {
      id: 'o4',
      sid: 's3',
      img: '/img/canteen/二三.png',
      sname: '二三',
      complete: '订单完成',
      orderLists: [{
        mid: "s3_2",
        name: '油爆大虾',
        num: 1,
        cost: 10
      }, {
        mid: "s3_1",
        name: '土豆丝',
        num: 1,
        cost: 10
      }],
      totalnum: 2,
      cost: 20
    }
    ]
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
    wx.setStorageSync('storesInfo', storesInfo)
    wx.setStorageSync('dishes', dishes)
    wx.setStorageSync('orders', orders)
  },
  globalData: {
    userInfo: null
  }
})